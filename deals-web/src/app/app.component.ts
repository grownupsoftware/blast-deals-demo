import {Component, OnInit} from '@angular/core';
import {GraphBlastService} from 'blast-graph-angular2/blast';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    blastService: GraphBlastService;
    deals: any = [];

    constructor() {
        // Step 1 - connect to blast server
        this.blastService = new GraphBlastService('ws://127.0.0.1:8081/blast');

        // Step 2 - attach graph's collection 'deals' to local array
        this.blastService.attach('deals', this.deals);

        // Step 3 - (Optional) callback for traffic light
        /**
         * Callback to manage traffic light
         */
        this.blastService.onOpen((msg: any) => {
            changeLight('green');
        });

        /**
         * Callback when server is disconnected
         */
        this.blastService.onClose((msg: any) => {
            changeLight('red');
        });

        function changeLight(color: any) {
            const trafficLightDiv = document.getElementById('trafficlight');
            trafficLightDiv.className = 'light ' + color;
        }

    }
    ngOnInit() {
        // turn splash - off
        const splash = document.getElementById('splash');
        if (splash) {
            splash.setAttribute('style', 'display:none;');
        }
    }

}
