webpackJsonp([1,4],{

/***/ 132:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(46)();
// imports


// module
exports.push([module.i, ":host .header{\n    background-color: lightgrey;\n    position: fixed;\n    top:0;\n    left:0;\n    display:inline-block;\n    width:100%;\n    height:50px;\n    border-bottom: 1px solid darkgrey;\n    z-index: 99999;\n}\n:host .logo {\n    position: fixed;\n    top: 0px;\n    left: 20px;\n}\n:host .title{\n    position: fixed;\n    top: 0px;\n    left: 85px;\n    color: rgb(18, 118, 149);\n    font-size: 36px;\n}\n:host .main-page {\n    position: absolute;\n    top: 50px;\n    left: 0px;\n    margin: 10px 5px 5px 10px;\n/*    height: 800px;*/\n    overflow-y: auto;\n}\n\n.image {\n    display: block;\n    -ms-flex-line-pack: center;\n        align-content: center;\n    margin:auto;\n    height:50%;\n}\n.deal {\n    display: inline-table;\n    margin: 10px;\n    /*    padding:10px 5px 5px 5px;*/\n    width: 200px;\n    height: 240px;\n    background: white;\n    border-radius: 10px;\n    box-shadow:         0px 0px 8px rgba(0,0,0,0.3);\n    position: relative;\n    z-index: 90;\n    font-family: Helvetica;\n}\n\n.ribbon-wrapper {\n    width: 85px;\n    height: 88px;\n    overflow: hidden;\n    position: absolute;\n    top: -3px;\n    right: -3px;\n}\n\n\n\n.ribbon {\n    font: bold 15px Sans-Serif;\n    color: #333;\n    text-align: center;\n    text-shadow: rgba(255,255,255,0.5) 0px 1px 0px;\n    -webkit-transform: rotate(45deg);\n    -moz-transform:    rotate(45deg);\n    -ms-transform:     rotate(45deg);\n    -o-transform:      rotate(45deg);\n    position: relative;\n    padding: 7px 0;\n    left: -5px;\n    top: 15px;\n    width: 120px;\n    color: #6a6340;\n    box-shadow:         0px 0px 3px rgba(0,0,0,0.3);\n}\n\n.ribbon:before, .ribbon:after {\n    content: \"\";\n    border-left:  3px solid transparent;\n    border-right: 3px solid transparent;\n    position:absolute;\n    bottom: -3px;\n}\n.starting {\n    background-color: #BFDC7A; \n    background-image:      -o-linear-gradient(top, #BFDC7A, #8EBF45); \n    font-size: 0.8em;\n}\n\n.closing {\n    background-color: #eaaa43; \n    background-image:      -o-linear-gradient(top, #eaaa43, #ea7c2c); \n}\n.closed {\n    background-color: #bf0e15; \n    background-image:      -o-linear-gradient(top, #bf0e15, #cc0a28); \n    color: #fff;\n}\n\n.starting:before, .starting:after {\n    border-top:   3px solid #6e8900;   \n}\n.closing:before, .closing:after {\n    border-top:   3px solid #9a690e;   \n}\n.closed:before, .closed:after {\n    border-top:   3px solid #9d0c33;   \n}\n\n.ribbon:before {\n    left: 0;\n}\n.ribbon:after {\n    right: 0;\n}\n.name {\n    margin:0px;\n    width:100%;\n    text-align: center;\n    background-color: lightgray;\n    border-top: solid 1px darkgray;\n    border-bottom: solid 1px darkgray;\n}\n.description {\n    font-size:0.8em;\n    color:#585a56;\n    margin:5px 10px 5px 10px;\n    word-wrap: break-word;\n    height:45px;\n}\n.price-wrapper {\n    position:absolute;\n    bottom:5px;\n    width:100%;\n    text-align: center;\n}\n.price {\n    background-color: #4d509c;\n    border: 1px solid #ba8def;\n    border-radius: 4px;\n    color:#fff;\n}\n:host .progress {\n    height:10px;\n    margin-bottom:5px;\n}\n:host .progress-STARTING {\n    background-color: #BFDC7A;\n}\n:host .progress-STARTED {\n    background-color: #BFDC7A;\n}\n:host .progress-CLOSING {\n    background-color: #eaaa43;\n}\n:host .progress-CLOSED {\n    background-color: #bf0e15;\n}\n:host .trafficlight{\n    display:inline;\n    float:right;\n    margin-right:20px;\n    margin-top:10px;\n}\n:host .trafficlight .heading{\n    color: rgb(18, 118, 149);\n    vertical-align: middle;\n    display:inline-block;\n    height:40px;\n}\n\n:host .light{\n    display:inline-block;\n    background-size: 5px 5px; \n    width: 30px;\n    height: 30px;\n    border-radius: 50%;\n}\n\n:host .red{\n    background: red;\n    background-image: radial-gradient(brown, transparent);\n    border: dotted 2px orange;\n    box-shadow: \n        0 0 5px #111 inset,\n        0 0 2px red;\n}\n\n:host .yellow{\n    background: yellow;\n    background-image: radial-gradient(orange, transparent);\n    border: dotted 2px yellow;\n    box-shadow: \n        0 0 5px #111 inset,\n        0 0 2px yellow;\n}\n\n:host .green{\n    background: green;\n    background-image: radial-gradient(lime, transparent);\n    border: dotted 2px lime;\n    box-shadow: \n        0 0 5px #111 inset,\n        0 0 2px lime;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 134:
/***/ (function(module, exports) {

module.exports = "<div class=\"header\">\n    <div class=\"logo\">\n        <img src=\"assets/b-circle-trans-100.png\" alt=\"logo\" width=\"50\" height=\"46\" >\n    </div>\n\n    <div class=\"title\">Black Friday Deals!</div>\n\n    <!-- a connection status traffic light -->\n    <div class=\"trafficlight\">\n        <div class=\"heading\">Connection Status: </div>\n        <div class=\"light red\" id=\"trafficlight\"></div>\n    </div>\n</div>\n\n<div class=\"main-page\">\n    <div *ngFor=\"let deal of deals\" class=\"deal\">\n        <div class=\"ribbon-wrapper\">\n            <div *ngIf=\"deal.state === 'STARTING'\" class=\"ribbon starting\">Coming Soon</div>\n            <div *ngIf=\"deal.state === 'CLOSING'\" class=\"ribbon closing\">Closing</div>\n            <div *ngIf=\"deal.state === 'CLOSED'\" class=\"ribbon closed\">Closed</div>\n        </div>\n        <img class=\"image\" src=\"{{deal.product.image}}\" alt=\"\"/>\n        <div class=\"name\">{{deal.product.name}}</div>\n        <div class=\"description\">{{deal.product.description}}</div>\n        \n        <div class=\"progress\" *ngIf=\"deal.progress > 0 \">\n            <div class=\"progress-bar progress-bar-success progress-bar-striped\" \n                 role=\"progressbar\" [attr.aria-valuenow]=\"deal.progress\" aria-valuemin=\"0\" aria-valuemax =\"100\" \n                 style=\"min-width: 2em;\"\n                 [ngClass]=\"'progress-' + deal.state\" \n                 [style.width]=\"(deal.progress/100)*100 + '%'\">\n            </div>\n        </div>\n\n        <div class=\"price-wrapper\">\n            <div class=\"price\">£ {{deal.product.price}}</div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ 156:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

/**
 * blast-angular2
 */
var Subject_1 = __webpack_require__(26);
var blast_helpers_1 = __webpack_require__(67);
exports.BLAST_VERSION = '0.0.1';
exports.LOG_LEVEL = {
    DEBUG: 0,
    INFO: 1,
    WARN: 2,
    ERROR: 3
};
var BlastService = (function () {
    function BlastService(url, connectNow, protocols, config) {
        this.url = url;
        this.protocols = protocols;
        this.config = config;
        this.reconnectAttempts = 0;
        this.sendQueue = [];
        this.onMessageCallbacks = [];
        this.onOpenCallbacks = [];
        this.onErrorCallbacks = [];
        this.onCloseCallbacks = [];
        this.readyStateConstants = {
            'CONNECTING': 0,
            'OPEN': 1,
            'CLOSING': 2,
            'CLOSED': 3,
            'RECONNECT_ABORTED': 4
        };
        this.normalCloseCode = 1000;
        this.reconnectableStatusCodes = [4000];
        this.logLevel = exports.LOG_LEVEL.ERROR;
        var match = new RegExp('wss?:\/\/').test(url);
        if (!match) {
            throw new Error('Invalid url provided');
        }
        this.config = config || { initialTimeout: 500, maxTimeout: 300000, reconnectIfNotNormalClose: true };
        this.dataStream = new Subject_1.Subject();
        if (connectNow === undefined || connectNow) {
            this.connect(true);
        }
    }
    BlastService.prototype.connect = function (force) {
        var _this = this;
        if (force === void 0) { force = false; }
        var self = this;
        if (force || !this.socket || this.socket.readyState !== this.readyStateConstants.OPEN) {
            self.socket = this.protocols ? new WebSocket(this.url, this.protocols) : new WebSocket(this.url);
            self.socket.onopen = function (ev) {
                _this.onOpenHandler(ev);
            };
            self.socket.onmessage = function (ev) {
                if (blast_helpers_1.BlastHelpers.isJson(ev.data)) {
                    var message = JSON.parse(ev.data);
                    _this.debug('BlastService', 'jsonMessage', 'passing to handlers', ev.data);
                    // call a json message handler - if true, then message handled mustn't carry on
                    if (self.handleJsonMessage(message) === true) {
                        return;
                    }
                }
                self.onMessageHandler(ev.data);
                _this.dataStream.next(ev.data);
            };
            this.socket.onclose = function (ev) {
                self.onCloseHandler(ev);
            };
            this.socket.onerror = function (ev) {
                self.onErrorHandler(ev);
                _this.dataStream.error(ev);
            };
        }
    };
    /**
     * Run in Block Mode
     * Return true when can send and false in socket closed
     * @param data
     * @returns {boolean}
     */
    BlastService.prototype.sendMessage = function (data, binary) {
        var self = this;
        if (this.getReadyState() !== this.readyStateConstants.OPEN
            && this.getReadyState() !== this.readyStateConstants.CONNECTING) {
            this.connect();
        }
        this.debug('BlastService', 'sendMessage', data);
        self.sendQueue.push({ message: data, binary: binary });
        if (self.socket.readyState === self.readyStateConstants.OPEN) {
            self.fireQueue();
            return true;
        }
        else {
            return false;
        }
    };
    /**
     * Use {mode} mode to send {data} data
     * If no specify, Default SendMode is Observable mode
     * @param data
     * @param mode
     * @param binary
     * @returns {any}
     */
    BlastService.prototype.send = function (data, binary) {
        return this.sendMessage(data, binary);
    };
    BlastService.prototype.getDataStream = function () {
        return this.dataStream;
    };
    BlastService.prototype.notifyOpenCallbacks = function (event) {
        for (var i = 0; i < this.onOpenCallbacks.length; i++) {
            this.onOpenCallbacks[i].call(this, event);
        }
    };
    BlastService.prototype.fireQueue = function () {
        while (this.sendQueue.length && this.socket.readyState === this.readyStateConstants.OPEN) {
            var data = this.sendQueue.shift();
            if (data.binary) {
                this.socket.send(data.message);
            }
            else {
                this.socket.send(blast_helpers_1.BlastHelpers.isString(data.message) ? data.message : JSON.stringify(data.message));
            }
        }
    };
    BlastService.prototype.notifyCloseCallbacks = function (event) {
        for (var i = 0; i < this.onCloseCallbacks.length; i++) {
            this.onCloseCallbacks[i].call(this, event);
        }
    };
    BlastService.prototype.notifyErrorCallbacks = function (event) {
        for (var i = 0; i < this.onErrorCallbacks.length; i++) {
            this.onErrorCallbacks[i].call(this, event);
        }
    };
    BlastService.prototype.onOpen = function (cb) {
        this.onOpenCallbacks.push(cb);
        return this;
    };
    ;
    BlastService.prototype.onClose = function (cb) {
        this.onCloseCallbacks.push(cb);
        return this;
    };
    BlastService.prototype.onError = function (cb) {
        this.onErrorCallbacks.push(cb);
        return this;
    };
    ;
    BlastService.prototype.onMessage = function (callback, options) {
        if (!blast_helpers_1.BlastHelpers.isFunction(callback)) {
            throw new Error('Callback must be a function');
        }
        this.onMessageCallbacks.push({
            fn: callback,
            pattern: options ? options.filter : undefined,
            autoApply: options ? options.autoApply : true
        });
        return this;
    };
    BlastService.prototype.handleJsonMessage = function (message) {
        // as a default return false i.e. don't change message flow
        // enables extended classes to override this function
        return false;
    };
    BlastService.prototype.onMessageHandler = function (message) {
        this.debug('BlastService', 'onMessageHandler', message.data);
        var self = this;
        var currentCallback;
        for (var i = 0; i < self.onMessageCallbacks.length; i++) {
            currentCallback = self.onMessageCallbacks[i];
            currentCallback.fn.apply(self, [message]);
        }
    };
    ;
    BlastService.prototype.onOpenHandler = function (event) {
        this.debug('BlastService', 'connected');
        this.reconnectAttempts = 0;
        this.notifyOpenCallbacks(event);
        this.fireQueue();
    };
    BlastService.prototype.onCloseHandler = function (event) {
        this.debug('BlastService', 'closed');
        this.notifyCloseCallbacks(event);
        if ((this.config.reconnectIfNotNormalClose && event.code !== this.normalCloseCode)
            || this.reconnectableStatusCodes.indexOf(event.code) > -1) {
            this.reconnect();
        }
        else {
            this.sendQueue = [];
            this.dataStream.complete();
        }
    };
    ;
    BlastService.prototype.onErrorHandler = function (event) {
        this.debug('BlastService', 'onErrorHandler', event);
        this.notifyErrorCallbacks(event);
    };
    ;
    BlastService.prototype.reconnect = function () {
        var _this = this;
        this.close(true);
        var backoffDelay = this.getBackoffDelay(++this.reconnectAttempts);
        //         let backoffDelaySeconds = backoffDelay / 1000;
        //         // console.log('Reconnecting in ' + backoffDelaySeconds + ' seconds');
        this.debug('BlastService', 'reconnectDelay', backoffDelay);
        setTimeout(function () { return _this.connect(); }, backoffDelay);
        return this;
    };
    BlastService.prototype.close = function (force) {
        if (force === void 0) { force = false; }
        if (force || !this.socket.bufferedAmount) {
            this.socket.close(this.normalCloseCode);
        }
        return this;
    };
    ;
    BlastService.prototype.getBackoffDelay = function (attempt) {
        var R = Math.random() + 1;
        var T = this.config.initialTimeout;
        var F = 2;
        var N = attempt;
        var M = this.config.maxTimeout;
        return Math.floor(Math.min(R * T * Math.pow(F, N), M));
    };
    ;
    BlastService.prototype.setInternalState = function (state) {
        if (Math.floor(state) !== state || state < 0 || state > 4) {
            throw new Error('state must be an integer between 0 and 4, got: ' + state);
        }
        this.internalConnectionState = state;
    };
    /**
     * Could be -1 if not initzialized yet
     * @returns {number}
     */
    BlastService.prototype.getReadyState = function () {
        if (this.socket == null) {
            return -1;
        }
        return this.internalConnectionState || this.socket.readyState;
    };
    BlastService.prototype.getVersion = function () {
        return exports.BLAST_VERSION;
    };
    BlastService.prototype.hasConsole = function () {
        if (console === undefined) {
            return false;
        }
        return true;
    };
    BlastService.prototype.debug = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (this.hasConsole() && this.logLevel < 1) {
            console.debug.apply(console, args);
        }
    };
    BlastService.prototype.info = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (this.hasConsole() && this.logLevel < 2) {
            console.debug.apply(console, args);
        }
    };
    BlastService.prototype.warn = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        if (this.hasConsole() && this.logLevel < 4) {
            console.debug.apply(console, args);
        }
    };
    BlastService.prototype.error = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        console.error.apply(console, args);
    };
    BlastService.prototype.setLogLevel = function (level) {
        this.logLevel = level;
    };
    return BlastService;
}());
exports.BlastService = BlastService;


/***/ }),

/***/ 157:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var BlastException = (function () {
    function BlastException(message) {
        this.message = message;
    }
    return BlastException;
}());
exports.BlastException = BlastException;


/***/ }),

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var blast_1 = __webpack_require__(20);
var graph_request_1 = __webpack_require__(161);
var path_details_1 = __webpack_require__(28);
var collection_traversor_1 = __webpack_require__(68);
var operation_1 = __webpack_require__(164);
var graph_response_message_1 = __webpack_require__(162);
var graph_message_type_1 = __webpack_require__(69);
var client_collection_1 = __webpack_require__(160);
var GraphBlastService = (function (_super) {
    __extends(GraphBlastService, _super);
    function GraphBlastService(_url, _connectNow, _protocols, _config) {
        var _this = _super.call(this, _url, _connectNow, _protocols, _config) || this;
        _this._url = _url;
        _this._connectNow = _connectNow;
        _this._protocols = _protocols;
        _this._config = _config;
        _this._correlatedGraphRequestMap = [];
        _this._collectionMap = {};
        _this._correlationId = 0;
        return _this;
    }
    GraphBlastService.prototype.add = function (collection, entity) {
        return this.sendGraphRequest(new graph_request_1.GraphRequest('add', collection, entity));
    };
    GraphBlastService.prototype.update = function (key, entity) {
        return this.sendGraphRequest(new graph_request_1.GraphRequest('update', key, entity));
    };
    GraphBlastService.prototype.remove = function (key) {
        return this.sendGraphRequest(new graph_request_1.GraphRequest('remove', key));
    };
    GraphBlastService.prototype.randomId = function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    };
    GraphBlastService.prototype.attach = function (key, data, parameters, modificationWatcher) {
        if (key.trim().length === 0) {
            key = 'root';
        }
        var isArray = false;
        if (Object.prototype.toString.call(data) === '[object Array]') {
            isArray = true;
        }
        // perform path and path->collection validation
        var pathDetails = path_details_1.PathDetails.splitPath(key);
        if (pathDetails[pathDetails.length - 1].getKeyField() !== undefined && isArray) {
            this.handlePromiseError(new blast_1.BlastException('Can only attach to a type of {} when keyField:keyValue is last element of path'));
            return;
        }
        if (pathDetails[pathDetails.length - 1].getKeyField() === undefined && !isArray) {
            this.handlePromiseError(new blast_1.BlastException('Can only attach to a type of [] when last element of path is a collection'));
            return;
        }
        var attachmentId = this.randomId();
        // add collection to map of collections
        var clientCollection = new client_collection_1.ClientCollection(attachmentId, key, modificationWatcher);
        if (isArray) {
            clientCollection.setListData(data);
        }
        else {
            clientCollection.setData(data);
        }
        this._collectionMap[attachmentId] = clientCollection;
        // console.log('added to collection', key, clientCollection, this._collectionMap);
        return this.sendGraphRequest(new graph_request_1.GraphRequest('attach', key, parameters, attachmentId));
    };
    GraphBlastService.prototype.handlePromiseError = function (blastException) {
        return new Promise(function (resolve, reject) {
            reject(blastException.message);
        });
        //        console.error('Exception:', blastException.message);
        //        const promise: Promise<any> = new Promise((onFulfilled, onRejected) => {
        //            onRejected(blastException.message);
        //        });
        //        return promise;
    };
    GraphBlastService.prototype.detach = function (key) {
        return this.sendGraphRequest(new graph_request_1.GraphRequest('detach', key));
    };
    GraphBlastService.prototype.detachAll = function () {
        return this.sendGraphRequest(new graph_request_1.GraphRequest('detachAll'));
    };
    GraphBlastService.prototype.getAttachments = function () {
        return this.sendGraphRequest(new graph_request_1.GraphRequest('attachments'));
    };
    GraphBlastService.prototype.fetch = function (key, parameters) {
        return this.sendGraphRequest(new graph_request_1.GraphRequest('fetch', key, parameters));
    };
    GraphBlastService.prototype.fetchRoot = function () {
        return this.sendGraphRequest(new graph_request_1.GraphRequest('fetch', ''));
    };
    GraphBlastService.prototype.getSchema = function () {
        return this.sendGraphRequest(new graph_request_1.GraphRequest('schema'));
    };
    GraphBlastService.prototype.sendGraphRequest = function (request) {
        var complete, err;
        //  create an empty response - we use the onFulFill and onReject functions on the response
        var promise = new Promise(function (onFulfilled, onRejected) {
            complete = onFulfilled;
            err = onRejected;
        });
        request.setCorrelationInfo(++this._correlationId, complete, err);
        //  we add the requestDO to a map - used to marry response with request
        this._correlatedGraphRequestMap[request._correlationId] = request;
        // console.log('request.getMessage()', request.getMessage());
        //  send the message
        this.send(request.getMessage());
        //  return the promise - will be fulfilled when we get a response from the server
        return promise;
    };
    GraphBlastService.prototype.buildPath = function (startIndex, pathDetails, addLastKey) {
        var builder = '';
        for (var x = startIndex; x < pathDetails.length; x++) {
            if (builder.length > 0) {
                builder = builder + '/';
            }
            builder = builder + pathDetails[x].getCollection();
            if (x < pathDetails.length - 1 || addLastKey) {
                builder = builder + '/';
                if (pathDetails[x].getKeyField() != null) {
                    builder = builder + pathDetails[x].getKeyField() + ':' + pathDetails[x].getKeyValue();
                }
            }
        }
        return builder;
    };
    GraphBlastService.prototype.calculateTruePath = function (key, path, operation) {
        var keyDetails = path_details_1.PathDetails.splitPath(key);
        var pathDetails = path_details_1.PathDetails.splitPath(path);
        var truePath = '';
        if (keyDetails[keyDetails.length - 1].isRoot()) {
            truePath = this.buildPath(0, pathDetails, operation === operation_1.Operation.UPDATE);
        }
        else if (keyDetails[keyDetails.length - 1].getKeyField() == null) {
            // collection is an array
            for (var x = 0; x < pathDetails.length; x++) {
                // loop through until collection in path matches last collection in key
                if (pathDetails[x].getCollection() === keyDetails[keyDetails.length - 1].getCollection()) {
                    truePath = this.buildPath(x, pathDetails, operation === operation_1.Operation.UPDATE);
                    break;
                }
            }
        }
        else {
            // collection is a map
            if (keyDetails.length === pathDetails.length) {
                truePath = this.buildPath(pathDetails.length - 1, pathDetails, operation === operation_1.Operation.UPDATE);
            }
            else {
                truePath = this.buildPath(keyDetails.length, pathDetails, operation === operation_1.Operation.UPDATE);
            }
        }
        return truePath;
    };
    GraphBlastService.prototype.applyChangeToRecord = function (collection, instruction) {
        for (var x = 0; x < instruction.getChanges().length; x++) {
            // name = name of field, value = new value
            collection[instruction.getChanges()[x]['name']] = instruction.getChanges()[x]['value'];
        }
    };
    GraphBlastService.prototype.getGraphMessage = function (value) {
        return new graph_response_message_1.GraphResponseMessage(JSON.parse(value));
    };
    GraphBlastService.prototype.handleInitialLoad = function (graphRequest, graphMessage) {
        //        const clientCollection: ClientCollection = this._collectionMap[graphMessage.getKey()];
        var clientCollection = this._collectionMap[graphMessage.getAttachmentId()];
        // console.log('retrieve [initial load] from collection', graphMessage.getAttachmentId(), clientCollection, this._collectionMap);
        if (clientCollection == null) {
            graphRequest._onError('Failed to find collection for key: ' + graphMessage.getAttachmentId());
            return;
        }
        if (clientCollection.isList()) {
            // as its initial load we can just add to collection
            if (graphMessage.getData() != null) {
                Array.prototype.push.apply(clientCollection.getListData(), graphMessage.getData());
                if (clientCollection.getModificationWatcher() !== undefined) {
                    clientCollection.getModificationWatcher().added(graphMessage.getData());
                }
                graphRequest._onFulFilled(clientCollection.getListData());
            }
        }
        else {
            this.mergeMap(clientCollection.getData(), graphMessage.getData());
            graphRequest._onFulFilled(clientCollection.getData());
        }
    };
    GraphBlastService.prototype.handleJsonMessage = function (message) {
        try {
            var graphMessage = new graph_response_message_1.GraphResponseMessage(message);
            this.handleCommand(graphMessage);
            return true;
        }
        catch (blastException) {
            // not a valid graph message, so return false so can be handled by normal flow
            console.error(blastException);
            return false;
        }
    };
    GraphBlastService.prototype.handleCommand = function (graphMessage) {
        // console.log('handle command', graphMessage);
        try {
            // 1st pass - handle responses that don't have a future attached
            switch (graphMessage.getCommand()) {
                case graph_message_type_1.GraphMessageType.GRAPH_ADD_RESPONSE:
                case graph_message_type_1.GraphMessageType.GRAPH_UPDATE_RESPONSE:
                case graph_message_type_1.GraphMessageType.GRAPH_REMOVE_RESPONSE:
                    this.handleGraphModify(graphMessage);
                    return;
            }
            var graphRequest = this._correlatedGraphRequestMap[graphMessage.getCorrelationId()];
            if (graphRequest === undefined) {
                throw new blast_1.BlastException('Failed to find correlation id: ' + graphMessage.getCorrelationId());
            }
            switch (graphMessage.getCommand()) {
                case graph_message_type_1.GraphMessageType.GRAPH_DETACH_RESPONSE:
                case graph_message_type_1.GraphMessageType.GRAPH_DETACH_ALL_RESPONSE:
                case graph_message_type_1.GraphMessageType.GRAPH_OK_RESPONSE:
                    graphRequest._onFulFilled(true);
                    break;
                case graph_message_type_1.GraphMessageType.GRAPH_FAIL_RESPONSE:
                    graphRequest._onFulFilled(false);
                    break;
                case graph_message_type_1.GraphMessageType.GRAPH_INITIAL_LOAD_RESPONSE:
                    this.handleInitialLoad(graphRequest, graphMessage);
                    break;
                case graph_message_type_1.GraphMessageType.GRAPH_CLIENT_ATTACHMENTS_RESPONSE:
                case graph_message_type_1.GraphMessageType.GRAPH_SCHEMA_RESPONSE:
                case graph_message_type_1.GraphMessageType.GRAPH_FETCH_RESPONSE:
                    graphRequest._onFulFilled(graphMessage.getData());
                    break;
                case graph_message_type_1.GraphMessageType.GRAPH_ADD_RESPONSE:
                case graph_message_type_1.GraphMessageType.GRAPH_UPDATE_RESPONSE:
                case graph_message_type_1.GraphMessageType.GRAPH_REMOVE_RESPONSE:
                    break;
            }
        }
        catch (blastException) {
        }
    };
    GraphBlastService.prototype.handleGraphModify = function (graphMessage) {
        // console.log('Handling ...', graphMessage);
        if (graphMessage.getInstruction() === undefined
            || (graphMessage.getInstruction().getOperation() !== operation_1.Operation.REMOVE
                && (graphMessage.getInstruction().getChanges() === undefined || graphMessage.getInstruction().getChanges().length === 0)
                && graphMessage.getInstruction().getRecord() === undefined)) {
            // 'No Instruction or no changes - doing nothing'
            return;
        }
        var clientCollection = this._collectionMap[graphMessage.getAttachmentId()];
        // console.log('retrieve [modify] from collection', graphMessage.getAttachmentId(), clientCollection, this._collectionMap);
        if (clientCollection === undefined) {
            // 'Cannot find collection for {}', graphMessage.getKey());
            return;
        }
        var path = this.calculateTruePath(graphMessage.getKey(), graphMessage.getInstruction().getPath(), graphMessage.getInstruction().getOperation());
        var parentList = [];
        var record = {};
        // console.log('Ready to ...', graphMessage.getInstruction().getOperation());
        switch (graphMessage.getInstruction().getOperation()) {
            case operation_1.Operation.ADD:
                // console.log('Okay Im Adding');
                if (clientCollection.isList()) {
                    collection_traversor_1.CollectionTraversor.findList(path, clientCollection.getListData()).push(graphMessage.getInstruction().getRecord());
                }
                else {
                    collection_traversor_1.CollectionTraversor.findList(path, clientCollection.getData()).push(graphMessage.getInstruction().getRecord());
                }
                if (clientCollection.getModificationWatcher() !== undefined) {
                    clientCollection.getModificationWatcher().added(graphMessage.getInstruction().getRecord());
                }
                break;
            case operation_1.Operation.UPDATE:
                // console.log('Okay Im Updating');
                if (clientCollection.isList()) {
                    record = collection_traversor_1.CollectionTraversor.findRecord(path, clientCollection.getListData());
                    this.applyChangeToRecord(record, graphMessage.getInstruction());
                    if (clientCollection.getModificationWatcher() !== undefined) {
                        clientCollection.getModificationWatcher().changed(record);
                    }
                }
                else {
                    if (path.length === 0 || graphMessage.getKey().endsWith(path)) {
                        // if key =  markets/id:101/runners/id:103 and path = runners/id:103
                        // then data is not actually hierarchal
                        record = clientCollection.getData();
                    }
                    else {
                        record = collection_traversor_1.CollectionTraversor.findRecord(path, clientCollection.getData());
                    }
                    this.applyChangeToRecord(record, graphMessage.getInstruction());
                    if (clientCollection.getModificationWatcher() !== undefined) {
                        clientCollection.getModificationWatcher().changed(record);
                    }
                }
                break;
            case operation_1.Operation.REMOVE:
                // console.log('Okay Im Removing');
                var pathDetails = path_details_1.PathDetails.splitPath(graphMessage.getInstruction().getPath());
                var recordKey = pathDetails[pathDetails.length - 1].getCollection() + '/'
                    + pathDetails[pathDetails.length - 1].getKeyField() + ':'
                    + pathDetails[pathDetails.length - 1].getKeyValue();
                var recordIndex = void 0;
                if (clientCollection.isList()) {
                    parentList = clientCollection.getListData();
                    // record = CollectionTraversor.findRecord(recordKey, clientCollection.getListData());
                    recordIndex = collection_traversor_1.CollectionTraversor.findRecordIndexInList(pathDetails[pathDetails.length - 1].getKeyField(), pathDetails[pathDetails.length - 1].getKeyValue(), clientCollection.getListData());
                }
                else {
                    parentList = collection_traversor_1.CollectionTraversor.findList(path, clientCollection.getData());
                    if (recordKey.length === 0 || graphMessage.getKey().endsWith(recordKey)) {
                        // record = clientCollection.getData();
                        recordIndex = 0;
                    }
                    else {
                        // record = CollectionTraversor.findRecord(recordKey, clientCollection.getData());
                        recordIndex = collection_traversor_1.CollectionTraversor.findRecordIndexInList(pathDetails[pathDetails.length - 1].getKeyField(), pathDetails[pathDetails.length - 1].getKeyValue(), parentList);
                    }
                }
                // parentList.remove(record);
                parentList.splice(recordIndex, 1);
                if (clientCollection.getModificationWatcher() != null) {
                    clientCollection.getModificationWatcher().removed(record);
                }
                break;
        }
    };
    GraphBlastService.prototype.mergeMap = function (baseObject, changedObject) {
        for (var p in changedObject) {
            if (changedObject.hasOwnProperty(p)) {
                try {
                    // Property in destination object set; update its value.
                    if (changedObject[p].constructor === Object) {
                        baseObject[p] = this.mergeMap(baseObject[p], changedObject[p]);
                    }
                    else {
                        baseObject[p] = changedObject[p];
                    }
                }
                catch (e) {
                    // Property in destination object not set; create it and set its value.
                    baseObject[p] = changedObject[p];
                }
            }
        }
    };
    return GraphBlastService;
}(blast_1.BlastService));
exports.GraphBlastService = GraphBlastService;


/***/ }),

/***/ 159:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var blast_1 = __webpack_require__(20);
exports.BlastHelpers = blast_1.BlastHelpers;
exports.BlastException = blast_1.BlastException;
exports.BlastService = blast_1.BlastService;
var blast_graph_angular2_1 = __webpack_require__(158);
exports.GraphBlastService = blast_graph_angular2_1.GraphBlastService;
var collection_traversor_1 = __webpack_require__(68);
exports.CollectionTraversor = collection_traversor_1.CollectionTraversor;
var path_parameters_1 = __webpack_require__(165);
exports.PathParameters = path_parameters_1.PathParameters;
var path_details_1 = __webpack_require__(28);
exports.PathDetails = path_details_1.PathDetails;
var query_parameter_1 = __webpack_require__(166);
exports.QueryParameter = query_parameter_1.QueryParameter;


/***/ }),

/***/ 160:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var ClientCollection = (function () {
    function ClientCollection(attachmentId, key, modificationWatcher) {
        this._key = key;
        this._modificationWatcher = modificationWatcher;
        this._attachmentId = attachmentId;
    }
    ClientCollection.prototype.isList = function () {
        return this._isList;
    };
    ClientCollection.prototype.getAttachmentId = function () {
        return this._attachmentId;
    };
    ClientCollection.prototype.getKey = function () {
        return this._key;
    };
    ClientCollection.prototype.getData = function () {
        return this._data;
    };
    ClientCollection.prototype.getListData = function () {
        return this._listData;
    };
    ClientCollection.prototype.getModificationWatcher = function () {
        return this._modificationWatcher;
    };
    ClientCollection.prototype.setData = function (data) {
        this._data = data;
        this._isList = false;
    };
    ClientCollection.prototype.setListData = function (data) {
        this._listData = data;
        this._isList = true;
    };
    return ClientCollection;
}());
exports.ClientCollection = ClientCollection;


/***/ }),

/***/ 161:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var path_details_1 = __webpack_require__(28);
var GraphRequest = (function () {
    // body?: any,
    function GraphRequest(command, key, pathParameters, attachmentId) {
        if (key !== undefined) {
            // validate key
            path_details_1.PathDetails.splitPath(key);
        }
        this._command = command;
        this._key = key;
        // this._body = body;
        this._pathParameters = pathParameters;
        this._attachmentId = attachmentId;
    }
    GraphRequest.prototype.setCommand = function (command) {
        this._command = command;
    };
    GraphRequest.prototype.setKey = function (key) {
        this._key = key;
    };
    GraphRequest.prototype.setParameters = function (pathParameters) {
        this._pathParameters = pathParameters;
    };
    // setBody(body: any) {
    // this._body = body;
    // }
    GraphRequest.prototype.setCollection = function (collection) {
        this._collection = collection;
    };
    GraphRequest.prototype.setCollectionList = function (collectionList) {
        this._collectionList = collectionList;
    };
    GraphRequest.prototype.setCorrelationInfo = function (correlationId, onFulFilled, onError) {
        this._correlationId = correlationId;
        this._onFulFilled = onFulFilled;
        this._onError = onError;
        this._requestTime = new Date().getTime();
    };
    GraphRequest.prototype.getMessage = function () {
        var message = {
            cmd: this._command,
            correlationId: this._correlationId
        };
        if (this._key !== undefined) {
            message['key'] = this._key;
        }
        if (this._pathParameters !== undefined) {
            message['parameters'] = this._pathParameters.getParameters();
        }
        if (this._attachmentId !== undefined) {
            message['attachmentId'] = this._attachmentId;
        }
        return message;
    };
    return GraphRequest;
}());
exports.GraphRequest = GraphRequest;


/***/ }),

/***/ 162:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var instruction_1 = __webpack_require__(163);
var blast_1 = __webpack_require__(20);
var graph_message_type_1 = __webpack_require__(69);
var GraphResponseMessage = (function () {
    function GraphResponseMessage(jsonObject) {
        if (jsonObject['graphMessageType'] === undefined) {
            throw new blast_1.BlastException('not a graph response message');
        }
        this._graphMessageType = jsonObject['graphMessageType'];
        if (graph_message_type_1.GraphMessageType[this._graphMessageType] === undefined) {
            throw new blast_1.BlastException('not a valid graph response message type');
        }
        this._correlationId = jsonObject['correlationId'];
        this._key = jsonObject['key'];
        if (jsonObject['instruction'] !== undefined) {
            this._instruction = new instruction_1.Instruction(jsonObject['instruction']);
        }
        this._data = jsonObject['data'];
        this._status = jsonObject['status'];
        this._cmd = jsonObject['cmd'];
        this._attachmentId = jsonObject['attachmentId'];
        this._graphMessageType = jsonObject['graphMessageType'];
    }
    GraphResponseMessage.prototype.getCorrelationId = function () {
        return this._correlationId;
    };
    GraphResponseMessage.prototype.getKey = function () {
        return this._key;
    };
    GraphResponseMessage.prototype.getInstruction = function () {
        return this._instruction;
    };
    GraphResponseMessage.prototype.getData = function () {
        return this._data;
    };
    GraphResponseMessage.prototype.getStatus = function () {
        return this._status;
    };
    GraphResponseMessage.prototype.getGraphMessageType = function () {
        return this._graphMessageType;
    };
    GraphResponseMessage.prototype.getCommand = function () {
        return this._cmd;
    };
    GraphResponseMessage.prototype.getAttachmentId = function () {
        return this._attachmentId;
    };
    return GraphResponseMessage;
}());
exports.GraphResponseMessage = GraphResponseMessage;


/***/ }),

/***/ 163:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var Instruction = (function () {
    function Instruction(jsonObject) {
        this._operation = jsonObject['operation'];
        this._path = jsonObject['path'];
        this._collection = jsonObject['collection'];
        this._changes = jsonObject['changes'];
        this._record = jsonObject['record'];
    }
    Instruction.prototype.getOperation = function () {
        return this._operation;
    };
    Instruction.prototype.getPath = function () {
        return this._path;
    };
    Instruction.prototype.getCollection = function () {
        return this._collection;
    };
    Instruction.prototype.getChanges = function () {
        return this._changes;
    };
    Instruction.prototype.getRecord = function () {
        return this._record;
    };
    return Instruction;
}());
exports.Instruction = Instruction;


/***/ }),

/***/ 164:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.Operation = {
    ADD: 'ADD',
    UPDATE: 'UPDATE',
    REMOVE: 'REMOVE'
};


/***/ }),

/***/ 165:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var PathParameters = (function () {
    function PathParameters(includeChildren, fields, predicates) {
        this._includeChildren = true;
        this._fields = [];
        this._predicates = [];
        if (includeChildren !== undefined) {
            this._includeChildren = includeChildren;
        }
        else {
            this._includeChildren = true;
        }
        if (fields !== undefined) {
            this._fields = fields;
        }
        if (predicates !== undefined) {
            this._predicates = predicates;
        }
    }
    PathParameters.prototype.getParameters = function () {
        var obj = {
            includeChildren: this._includeChildren
        };
        if (this._fields !== undefined) {
            obj['fields'] = this._fields;
        }
        if (this._predicates !== undefined) {
            obj['predicates'] = this._predicates;
        }
        return obj;
    };
    return PathParameters;
}());
exports.PathParameters = PathParameters;


/***/ }),

/***/ 166:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var QueryParameter = (function () {
    function QueryParameter(field, operand, value) {
        this._field = field;
        this._operand = operand;
        this._value = value;
    }
    return QueryParameter;
}());
exports.QueryParameter = QueryParameter;


/***/ }),

/***/ 169:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(71);


/***/ }),

/***/ 20:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var blast_helpers_1 = __webpack_require__(67);
exports.BlastHelpers = blast_helpers_1.BlastHelpers;
var blast_exception_1 = __webpack_require__(157);
exports.BlastException = blast_exception_1.BlastException;
var blast_angular2_1 = __webpack_require__(156);
exports.BlastService = blast_angular2_1.BlastService;


/***/ }),

/***/ 28:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var blast_1 = __webpack_require__(20);
var PathDetails = (function () {
    function PathDetails() {
    }
    PathDetails.splitPath = function (key) {
        var details = [];
        if (key === null || key.trim().length === 0 || key === 'root') {
            // root
            var pathDetails_1 = new PathDetails();
            pathDetails_1.setRoot();
            pathDetails_1.setNotCollectionOnly();
            details.push(pathDetails_1);
            return details;
        }
        if (key.startsWith('/')) {
            throw new blast_1.BlastException('key cannot start with / - must specify collection e.g. collection/keyname:value');
        }
        var splits = key.split('/');
        if (splits[0].includes(':')) {
            throw new blast_1.BlastException('must specify collection e.g. collection/keyname:value');
        }
        // check if it is collection only
        if (splits.length === 1) {
            // collection
            var pathDetails_2 = new PathDetails();
            pathDetails_2.setNotRoot();
            pathDetails_2.setCollection(key);
            pathDetails_2.setCollectionOnly();
            details.push(pathDetails_2);
            return details;
        }
        var pathDetails = new PathDetails();
        pathDetails.setNotRoot();
        pathDetails.setCollection(splits[0]);
        details.push(pathDetails);
        for (var x = 1; x < splits.length; x++) {
            if (splits[x].includes(':')) {
                var keySplit = splits[x].split(':');
                if (keySplit.length !== 2) {
                    throw new blast_1.BlastException('look up must be in form keyname:value - found: ' + splits[x]);
                }
                pathDetails.setKeyField(keySplit[0]);
                pathDetails.setKeyValue(keySplit[1]);
                pathDetails.setNotCollectionOnly();
            }
            else {
                if (splits[x].startsWith('?')) {
                    pathDetails.setQueryParams(splits[x].substring(1).split(','));
                }
                else {
                    pathDetails = new PathDetails();
                    pathDetails.setNotRoot();
                    pathDetails.setCollectionOnly();
                    pathDetails.setCollection(splits[x]);
                    details.push(pathDetails);
                }
            }
        }
        return details;
    };
    PathDetails.prototype.setRoot = function () {
        this._isRoot = true;
        this._collection = 'root';
    };
    PathDetails.prototype.setNotRoot = function () {
        this._isRoot = false;
    };
    PathDetails.prototype.setCollection = function (collection) {
        this._collection = collection;
    };
    PathDetails.prototype.setCollectionOnly = function () {
        this._isCollectionOnly = true;
    };
    PathDetails.prototype.setNotCollectionOnly = function () {
        this._isCollectionOnly = false;
    };
    PathDetails.prototype.setKeyField = function (keyField) {
        this._keyField = keyField;
    };
    PathDetails.prototype.setKeyValue = function (keyValue) {
        this._keyValue = keyValue;
    };
    PathDetails.prototype.setQueryParams = function (params) {
        this._queryParams = params;
    };
    PathDetails.prototype.getCollection = function () {
        return this._collection;
    };
    PathDetails.prototype.getKeyField = function () {
        return this._keyField;
    };
    PathDetails.prototype.getKeyValue = function () {
        return this._keyValue;
    };
    PathDetails.prototype.getQueryParams = function () {
        return this._queryParams;
    };
    PathDetails.prototype.isRoot = function () {
        return this._isRoot;
    };
    PathDetails.prototype.isCollectionOnly = function () {
        return this._isCollectionOnly;
    };
    return PathDetails;
}());
exports.PathDetails = PathDetails;


/***/ }),

/***/ 67:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var BlastHelpers = (function () {
    function BlastHelpers() {
    }
    BlastHelpers.isPresent = function (obj) {
        return obj !== undefined && obj !== null;
    };
    BlastHelpers.isString = function (obj) {
        return typeof obj === 'string';
    };
    BlastHelpers.isArray = function (obj) {
        return Array.isArray(obj);
    };
    BlastHelpers.isFunction = function (obj) {
        return typeof obj === 'function';
    };
    BlastHelpers.isJson = function (obj) {
        try {
            JSON.parse(obj);
            return true;
        }
        catch (exception) {
            return false;
        }
    };
    return BlastHelpers;
}());
exports.BlastHelpers = BlastHelpers;
;


/***/ }),

/***/ 68:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var path_details_1 = __webpack_require__(28);
var blast_1 = __webpack_require__(20);
var CollectionTraversor = (function () {
    function CollectionTraversor() {
    }
    CollectionTraversor.findList = function (path, data) {
        // console.log('path',path,'data',data);
        if (Object.prototype.toString.call(data) === '[object Array]') {
            return this.traverseUntilFinalList(path, data);
        }
        else {
            var pathDetails = path_details_1.PathDetails.splitPath(path);
            if (pathDetails.length === 1 && pathDetails[0].getKeyField() === undefined) {
                return data[pathDetails[0].getCollection()];
            }
            var resultList = [];
            for (var index = 0; index < pathDetails.length; index++) {
                if (pathDetails[index].getKeyField() === undefined) {
                    resultList = data[pathDetails[index].getCollection()];
                }
                else {
                    data = this.findARecord(index, pathDetails, data);
                }
            }
            return resultList === undefined ? [] : resultList;
        }
    };
    CollectionTraversor.findRecord = function (path, data) {
        if (Object.prototype.toString.call(data) === '[object Array]') {
            return this.traverseUntilFinalRecord(path, data);
        }
        else {
            var pathDetails = path_details_1.PathDetails.splitPath(path);
            for (var index = 0; index < pathDetails.length; index++) {
                data = this.findARecord(index, pathDetails, data);
            }
            return data;
        }
    };
    CollectionTraversor.findRecordInList = function (keyField, keyValue, list) {
        if (list !== undefined) {
            for (var index = 0; index < list.length; index++) {
                var id = list[index][keyField];
                if (id !== undefined) {
                    if (this.matches(id, keyValue)) {
                        return list[index];
                    }
                }
            }
        }
        throw new blast_1.BlastException('[findRecordInList] failed to find record - field [' + keyField + ' ] value [' + keyValue + ']');
    };
    CollectionTraversor.findRecordIndexInList = function (keyField, keyValue, list) {
        if (list !== undefined) {
            for (var index = 0; index < list.length; index++) {
                var id = list[index][keyField];
                if (id !== undefined) {
                    if (this.matches(id, keyValue)) {
                        return index;
                    }
                }
            }
        }
        throw new blast_1.BlastException('[findRecordIndexInList] failed to find record - field [' + keyField + ' ] value [' + keyValue + ']');
    };
    CollectionTraversor.findARecord = function (index, pathDetails, dataSubset) {
        var list = dataSubset[pathDetails[index].getCollection()];
        return this.findRecordInList(pathDetails[index].getKeyField(), pathDetails[index].getKeyValue(), list);
    };
    CollectionTraversor.traverseUntilFinalRecord = function (path, list) {
        var pathDetails = path_details_1.PathDetails.splitPath(path);
        var data = this.findRecordInList(pathDetails[0].getKeyField(), pathDetails[0].getKeyValue(), list);
        for (var index = 1; index < pathDetails.length; index++) {
            data = this.findARecord(index, pathDetails, data);
        }
        return data;
    };
    CollectionTraversor.traverseUntilFinalList = function (path, list) {
        var pathDetails = path_details_1.PathDetails.splitPath(path);
        // asking for a list with no details so return original list
        if (pathDetails[0].getKeyField() === undefined) {
            return list;
        }
        var resultList = [];
        var data = this.findRecordInList(pathDetails[0].getKeyField(), pathDetails[0].getKeyValue(), list);
        for (var index = 1; index < pathDetails.length; index++) {
            // there is no key field or we are on last path element
            if (pathDetails[index].getKeyField() === undefined || index === pathDetails.length - 1) {
                resultList = data[pathDetails[index].getCollection()];
            }
            else {
                data = this.findARecord(index, pathDetails, data);
            }
        }
        return resultList === undefined ? [] : resultList;
    };
    CollectionTraversor.matches = function (one, two) {
        if (typeof (one) === 'number') {
            if (typeof (two) === 'number') {
                return one === two;
            }
            else {
                return one === Number(two);
            }
        }
        else {
            if (typeof (two) === 'string') {
                return one === two;
            }
            else {
                return one === String(two);
            }
        }
    };
    return CollectionTraversor;
}());
exports.CollectionTraversor = CollectionTraversor;


/***/ }),

/***/ 69:
/***/ (function(module, exports, __webpack_require__) {

"use strict";

exports.GraphMessageType = {
    GRAPH_ADD_RESPONSE: 'graph-add',
    GRAPH_UPDATE_RESPONSE: 'graph-update',
    GRAPH_REMOVE_RESPONSE: 'graph-remove',
    GRAPH_INITIAL_LOAD_RESPONSE: 'graph-attach',
    GRAPH_FETCH_RESPONSE: 'graph-fetch',
    GRAPH_SCHEMA_RESPONSE: 'graph-schema',
    GRAPH_CLIENT_ATTACHMENTS_RESPONSE: 'graph-current_attachments',
    GRAPH_OK_RESPONSE: 'ok',
    GRAPH_FAIL_RESPONSE: 'fail',
    GRAPH_DETACH_RESPONSE: 'graph-detach',
    GRAPH_DETACH_ALL_RESPONSE: 'graph-detach_all'
};


/***/ }),

/***/ 70:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 70;


/***/ }),

/***/ 71:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(78);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 76:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_blast_graph_angular2_blast__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_blast_graph_angular2_blast___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_blast_graph_angular2_blast__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = (function () {
    function AppComponent() {
        this.deals = [];
        // Step 1 - connect to blast server
        this.blastService = new __WEBPACK_IMPORTED_MODULE_1_blast_graph_angular2_blast__["GraphBlastService"]('ws://127.0.0.1:8081/blast');
        // Step 2 - attach graph's collection 'deals' to local array
        this.blastService.attach('deals', this.deals);
        // Step 3 - (Optional) callback for traffic light
        /**
         * Callback to manage traffic light
         */
        this.blastService.onOpen(function (msg) {
            changeLight('green');
        });
        /**
         * Callback when server is disconnected
         */
        this.blastService.onClose(function (msg) {
            changeLight('red');
        });
        function changeLight(color) {
            var trafficLightDiv = document.getElementById('trafficlight');
            trafficLightDiv.className = 'light ' + color;
        }
    }
    AppComponent.prototype.ngOnInit = function () {
        // turn splash - off
        var splash = document.getElementById('splash');
        if (splash) {
            splash.setAttribute('style', 'display:none;');
        }
    };
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Y" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__(134),
        styles: [__webpack_require__(132)]
    }),
    __metadata("design:paramtypes", [])
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_component__ = __webpack_require__(76);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */]
        ],
        providers: [],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 78:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ })

},[169]);
//# sourceMappingURL=main.bundle.js.map