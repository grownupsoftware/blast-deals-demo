/*
 * Grownup Software Limited.
 */
package blast.blackfriday.dealmanager;

import blast.blackfriday.model.CategoryDO;
import blast.blackfriday.model.DataLoadDO;
import blast.blackfriday.model.DealDO;
import blast.blackfriday.model.ProductDO;
import blast.exception.BlastException;
import blast.graph.core.BlastGraph;
import blast.graph.core.GraphCollection;
import blast.json.ObjectMapperFactory;
import blast.log.BlastLogger;
import blast.utils.BlastUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;

/**
 *
 * @author grant
 */
public class BlackFridayGraphBuilder {

    private final BlastLogger logger = BlastLogger.createLogger();

    private final BlastGraph theBlastGraph;

    public BlackFridayGraphBuilder(BlastGraph blastGraph) {
        this.theBlastGraph = blastGraph;
    }

    public void build() throws BlastException {
        GraphCollection<CategoryDO, Long> categories = theBlastGraph.createCollection()
                .name("categories")
                .keyfield("categoryId")
                .dataclass(CategoryDO.class)
                .build();

        // create markets collection
        GraphCollection<ProductDO, Long> products = theBlastGraph.createCollection()
                .name("products")
                .keyfield("productId")
                .dataclass(ProductDO.class)
                .build();

        // create runners collection
        GraphCollection<DealDO, Long> deals = theBlastGraph.createCollection()
                .name("deals")
                .keyfield("id")
                .dataclass(DealDO.class)
                .build();

        products.linkedTo(categories).using("categoryId");

        // load some data
        DataLoadDO dataLoadDO = loadData();

        // load the categories
        BlastUtils.safeStream(dataLoadDO.getCategories()).forEach(category -> {
            logger.info("loading category: {}", category.getName());

            try {
                categories.add(category);
            } catch (BlastException ex) {
                logger.error("Failed to add category: {}", category.getName(), ex);
            }
        });

        // load the products
        BlastUtils.safeStream(dataLoadDO.getProducts()).forEach(product -> {
            logger.info("loading product: {}", product.getName());
            try {
                products.add(product);
            } catch (BlastException ex) {
                logger.error("Failed to add product: {}", product.getName(), ex);
            }
        });

        // Step 3 - start the deal manager
        DealManager dealManager = new DealManager(theBlastGraph, dataLoadDO);
        dealManager.start();

    }

    public DataLoadDO loadData() throws BlastException {

        try {
            ObjectMapper objectMapper = ObjectMapperFactory.createObjectMapper();

            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("data.json").getFile());

            return objectMapper.readValue(file, DataLoadDO.class);
        } catch (IOException ex) {
            logger.error("Error loading data", ex);
            throw new BlastException("Error loading data", ex);
        }
    }

}
