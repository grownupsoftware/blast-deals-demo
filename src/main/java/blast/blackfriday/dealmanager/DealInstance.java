package blast.blackfriday.dealmanager;

import blast.blackfriday.model.DealDO;
import blast.blackfriday.model.DealState;
import blast.exception.BlastException;
import blast.graph.core.BlastGraph;
import blast.graph.core.utils.GraphUtils;
import blast.log.BlastLogger;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author grant
 */
public class DealInstance {

    private final BlastLogger logger = BlastLogger.createLogger();

    private final static int STARTED_TIME = 3;
    private final int CLOSING_TIME;
    private final int CLOSED_TIME;
    private final int REMOVE_TIME;

    private final AtomicInteger theTick = new AtomicInteger(0);
    private final AtomicInteger theProgress = new AtomicInteger(1);

    private final Random theRandom = new Random();

    private final Timer theTimer = new Timer();

    private final BlastGraph theBlastGraph;

    private final DealDO theDealDo;

    private boolean shutdown = false;

    public DealInstance(BlastGraph blastGraph, DealDO dealDo) {
        this.theBlastGraph = blastGraph;
        this.theDealDo = dealDo;

        CLOSING_TIME = ThreadLocalRandom.current().nextInt(5, 30) + STARTED_TIME;
        CLOSED_TIME = CLOSING_TIME + 3;
        REMOVE_TIME = CLOSED_TIME + 5;
    }

    public void start() {

        // add to graph
        try {
            logger.debug("Creating deal: {}", theDealDo.getId());
            theBlastGraph.add("deals", GraphUtils.objectToMap(theDealDo));
        } catch (BlastException ex) {
            logger.error("Failed to create new deal", ex);
            return;
        }

        // setup timer for each second
        theTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                onTick();
            }
        }, 0, 1000);

    }

    private void onTick() {
        int tick = theTick.getAndIncrement();

        // update progress bar
        if (tick >= CLOSED_TIME) {
            // finished
            theDealDo.setProgress(100);
        } else if (tick > STARTED_TIME) {
            int progress = theProgress.getAndIncrement();
            theDealDo.setProgress((int) ((progress * 1.0) / (CLOSED_TIME - STARTED_TIME) * 100.0));
        }

        if (tick < STARTED_TIME) {
            theDealDo.setState(DealState.STARTING);
        } else if (tick >= STARTED_TIME && tick < CLOSING_TIME) {
            theDealDo.setState(DealState.STARTED);
        } else if (tick >= CLOSING_TIME && tick < CLOSED_TIME) {
            theDealDo.setState(DealState.CLOSING);
        } else if (tick >= CLOSED_TIME && tick < REMOVE_TIME) {
            theDealDo.setState(DealState.CLOSED);
        } else {
            shutdownInstance();
        }

        if (!shutdown) {
            try {
                theBlastGraph.update("deals/id:" + theDealDo.getId(), GraphUtils.objectToMap(theDealDo));
            } catch (BlastException ex) {
                logger.error("Error updating deal: {}", theDealDo.getId(), ex);
            }
        }

    }

    private void shutdownInstance() {
        logger.debug("-- shutting down: {}", theDealDo.getId());
        shutdown = true;

        // remove deal from graph
        try {
            theBlastGraph.remove("deals/id:" + theDealDo.getId());
        } catch (BlastException ex) {
            logger.error("Error removing deal: {}", theDealDo.getId(), ex);
        }

        // close the timer
        theTimer.cancel();
    }

}
