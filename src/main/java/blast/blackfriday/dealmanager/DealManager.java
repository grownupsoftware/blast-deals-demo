/*
 * Grownup Software Limited.
 */
package blast.blackfriday.dealmanager;

import blast.blackfriday.model.CategoryDO;
import blast.blackfriday.model.DataLoadDO;
import blast.blackfriday.model.DealDO;
import blast.blackfriday.model.ProductDO;
import blast.exception.BlastException;
import blast.graph.core.BlastGraph;
import blast.log.BlastLogger;
import blast.utils.BlastUtils;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author grant
 */
public class DealManager {

    private static final int NUM_OPENING_DEALS = 4;
    private static final long SECONDS = 1000;

    private final BlastLogger logger = BlastLogger.createLogger();

    private final BlastGraph theBlastGraph;
    private final DataLoadDO theData;
    private final Random theRandom = new Random();
    private final Map<Long, CategoryDO> theCategoryMap;

    private final AtomicLong dealCounter = new AtomicLong(6000);
    private final Timer theTimer = new Timer();

    public DealManager(BlastGraph blastGraph, DataLoadDO dataLoadDO) {
        this.theBlastGraph = blastGraph;
        this.theData = dataLoadDO;

        theCategoryMap = BlastUtils.safeStream(theData.getCategories()).collect(Collectors.toMap(CategoryDO::getCategoryId, cat -> cat));
    }

    /**
     * Loads some initial deals and a scheduled task to create some more in the
     * future
     */
    public void start() {
        logger.info("-- Starting the deal manager -- ");

        // create some deals at start up
        for (int x = 0; x < NUM_OPENING_DEALS; x++) {
            try {
                createNewDeal();
            } catch (BlastException ex) {
                logger.error("Failed to create deal", ex);
            }
        }

        // every x seconds create a new deal
        theTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                try {
                    createNewDeal();
                } catch (BlastException ex) {
                    logger.error("Failed to create deal", ex);
                }
            }

        }, 5 * SECONDS, 5 * SECONDS);

    }

    /**
     * select a random product for the loaded products
     *
     * @return the random product
     */
    private ProductDO getRandomProduct() {
        int numProducts = theData.getProducts().size();
        int index = theRandom.nextInt(numProducts);
        return theData.getProducts().get(index);
    }

    /**
     * creates a new deal and timer tasks to change state
     */
    private void createNewDeal() throws BlastException {
        ProductDO productDo = getRandomProduct();
        CategoryDO categoryDo = theCategoryMap.get(productDo.getCategoryId());
        DealDO dealDo = new DealDO(dealCounter.getAndIncrement(), productDo, categoryDo);

        DealInstance dealInstance = new DealInstance(theBlastGraph, dealDo);
        dealInstance.start();

    }

}
