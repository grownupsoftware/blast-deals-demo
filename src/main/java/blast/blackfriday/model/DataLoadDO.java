/*
 * Grownup Software Limited.
 */
package blast.blackfriday.model;

import blast.doc.DocField;
import java.util.List;

/**
 * Base POJO for loading data from data.json
 *
 * @author grant
 */
public class DataLoadDO {

    @DocField(description = "The categories")
    private List<CategoryDO> categories;

    @DocField(description = "The products")
    private List<ProductDO> products;

    public DataLoadDO() {
    }

    public List<CategoryDO> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDO> categories) {
        this.categories = categories;
    }

    public List<ProductDO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDO> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "DataLoadDO{" + "categories=" + categories + ", products=" + products + '}';
    }

}
