/*
 * Grownup Software Limited.
 */
package blast.blackfriday.model;

/**
 *
 * @author grant
 */
public enum DealState {
    WAITING,
    STARTING,
    STARTED,
    CLOSING,
    CLOSED
}
