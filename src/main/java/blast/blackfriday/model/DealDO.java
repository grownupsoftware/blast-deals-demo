/*
 * Grownup Software Limited.
 */
package blast.blackfriday.model;

import blast.doc.DocField;

/**
 *
 * @author grant
 */
public class DealDO {

    @DocField(description = "Unique Identifier for the deal")
    private Long id;

    @DocField(description = "The product")
    private ProductDO product;

    @DocField(description = "The category")
    private CategoryDO category;

    @DocField(description = "The State of the deal")
    private DealState state;

    @DocField(description = "Progress")
    private int progress;

    public DealDO() {
    }

    public DealDO(Long dealId, ProductDO product, CategoryDO category) {
        this.id = dealId;
        this.product = product;
        this.category = category;
        state = DealState.WAITING;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long dealId) {
        this.id = dealId;
    }

    public ProductDO getProduct() {
        return product;
    }

    public void setProduct(ProductDO product) {
        this.product = product;
    }

    public CategoryDO getCategory() {
        return category;
    }

    public void setCategory(CategoryDO category) {
        this.category = category;
    }

    public DealState getState() {
        return state;
    }

    public void setState(DealState state) {
        this.state = state;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    @Override
    public String toString() {
        return "DealDO{" + "Id=" + id + ", product=" + product + ", category=" + category + ", state=" + state + '}';
    }

}
