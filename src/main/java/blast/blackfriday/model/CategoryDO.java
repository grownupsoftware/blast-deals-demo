/*
 * Grownup Software Limited.
 */
package blast.blackfriday.model;

import blast.doc.DocField;

/**
 *
 * @author grant
 */
public class CategoryDO {

    @DocField(description = "Unique Identifier")
    private Long categoryId;

    @DocField(description = "Category Name")
    private String name;

    @DocField(description = "Intro text for the category")
    private String description;

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "CategoryDO{" + "categoryId=" + categoryId + ", name=" + name + ", description=" + description + '}';
    }

}
