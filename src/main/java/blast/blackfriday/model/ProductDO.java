/*
 * Grownup Software Limited.
 */
package blast.blackfriday.model;

import blast.doc.DocField;

/**
 *
 * @author grant
 */
public class ProductDO {

    @DocField(description = "Unique Identifier")
    private Long productId;

    @DocField(description = "The Category Identifier")
    private Long categoryId;

    @DocField(description = "Producy Name")
    private String name;

    @DocField(description = "Product description")
    private String description;

    @DocField(description = "Product price")
    private Double price;

    @DocField(description = "Base64 string representation of product image")
    private String image;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "ProductDO{" + "productId=" + productId + ", categoryId=" + categoryId + ", name=" + name + ", description=" + description + ", price=" + price + ", image=" + image + '}';
    }

}
