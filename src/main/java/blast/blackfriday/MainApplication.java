package blast.blackfriday;

/*
 * Grownup Software Limited.
 */
import blast.Blast;
import static blast.BlastConstants.BLAST_ROUTE_PATH;
import blast.blackfriday.dealmanager.BlackFridayGraphBuilder;
import blast.exception.BlastException;
import blast.graph.core.BlastGraph;
import blast.graph.core.BlastGraphImpl;
import blast.graph.module.GraphModule;
import blast.server.BlastServer;
import blast.server.config.BlastProperties;
import blast.vertx.engine.BlastRequestHandler;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.StaticHandler;

/**
 *
 * @author grant
 */
public class MainApplication {

    public static void main(String[] args) {
        try {

            Vertx vertx = Vertx.vertx();
            Router router = Router.router(vertx);

            // create a new Blast Graph
            BlastGraph blastGraph = new BlastGraphImpl();

            // create the graph module with the graph
            GraphModule graphModule = new GraphModule(blastGraph);

            // configure the blast server
            BlastServer blastServer = Blast.blast(graphModule);

            // add server to blast graph
            blastGraph.setBlastServer(blastServer);

            // load data and start the deal manager
            BlackFridayGraphBuilder graphBuilder = new BlackFridayGraphBuilder(blastGraph);
            graphBuilder.build();

            BlastRequestHandler handler = new BlastRequestHandler(blastServer);

            //Bind Blast
            router.route(BLAST_ROUTE_PATH).handler(routingContext -> {
                handler.handle(routingContext.request());
            });

            // Add static handler for web content
            StaticHandler staticHandler = StaticHandler.create("src/main/resources/webapp/demo");
            staticHandler.setIndexPage("index.html");
            staticHandler.setCachingEnabled(false);

            router.route("/demo/*").handler(staticHandler);

            BlastProperties properties = blastServer.getProperties();
            HttpServerOptions options = new HttpServerOptions();
            options.setAcceptBacklog(properties.getEndpoint().getBacklog());
            options.setPort(properties.getEndpoint().getPort());
            // Don't time out, need to do something with ping here
            options.setIdleTimeout(0);
            options.setReuseAddress(properties.isReuseAddress()).setReceiveBufferSize(properties.getInputBufferSize());
            options.setSendBufferSize(properties.getOutputBufferSize()).setSoLinger(properties.getSoLinger());
            options.setTcpNoDelay(properties.isNoDelay());

            blastServer.startup();
            vertx.createHttpServer(options).requestHandler(router::accept).listen();

        } catch (BlastException ex) {
            Blast.logger.warn("Can't start Blast!", ex);
        }
    }

}
