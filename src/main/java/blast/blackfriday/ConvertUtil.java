package blast.blackfriday;

import blast.log.BlastLogger;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Base64;
import org.neo4j.kernel.impl.util.FileUtils;

/**
 * Utility class to read jpeg image and convert to base64 string
 * @author grant
 */
public class ConvertUtil {

    private static final BlastLogger logger = BlastLogger.createLogger();

    public static final String IMAGE_DIR = "/tmp/images";

    public static void main(String... args) {
        ConvertUtil.convertImages();
    }

    public static void convertImages() {
        StringBuilder builder = new StringBuilder();
        File dir = new File(IMAGE_DIR);
        builder.append("[");
        for (File imageFile : dir.listFiles()) {
            builder.append("{");
            try {
                String encodedfile = null;
//            try {
                FileInputStream fileInputStreamReader = new FileInputStream(imageFile);
                byte[] bytes = new byte[(int) imageFile.length()];
                fileInputStreamReader.read(bytes);

                encodedfile = new String(Base64.getEncoder().encode((bytes)), "UTF-8");
                builder.append("categoryId:");
                if (imageFile.getName().startsWith("tv")) {
                    builder.append("1,").append("\n");
                } else {
                    builder.append("2,").append("\n");
                }
                builder.append("name:").append("\"").append(imageFile.getName()).append("\",").append("\n");
                builder.append("description:").append("\"").append("").append("\",").append("\n");
                builder.append("price:").append("\"").append(5.99).append("\",").append("\n");
                builder.append("image:").append("\"data:image/jpg;base64,").append(encodedfile).append("\"").append("\n");

                builder.append("},").append("\n");
            } catch (IOException ex) {
                logger.error("Error", ex);
            }
        }

        builder.append("]");
        logger.info(builder.toString());
        try {
            FileUtils.writeToFile(new File("/tmp/output.json"), builder.toString(), false);
        } catch (IOException ex) {
            logger.error("Error", ex);
        }

    }
}
