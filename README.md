![Alt text](./resources/images/b-circle-trans-100.png)
# Blast-deals-demo
![Alt text](./resources/images/deals-demo.png)

## Getting Started

```
git clone
cd blast-deals-demo
gradle wrapper
./gradlew run

```
Then open the browser for the [demo](http://localhost:8081/demo/index.html)

#Overview

1. Products are loaded from a static data file (resources:data.json)
2. The 'Deal Manager' (server-side) every 5 seconds randomly selects a product and creates a 'deal'
3. The 'Deal' goes through the following states: Starting, Started, Closing and Closed.
4. The Deal also has a 'ticker' that indicates the time remaining for the deal
5. The web client attaches a local copy (variable) to the server side collection called 'deals'
6. HTML on the web is rendered from the local copy (or NgModel in Angular terminlogy)
7. As the ticker is updated or, a state for a deal changed by the 'Deal Manager', then the server pushes these changes to all connected clients.
8. The local copy is automaticaaly updated and the HTML code *reacts* to these changes. 



#Client Code

```
import {Component, OnInit} from '@angular/core';
import {GraphBlastService} from 'blast-graph-angular2/blast';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    blastService: GraphBlastService;

    // the variable that will hold the local copy
    deals: any = [];

    constructor() {
        // Step 1 - connect to blast server
        this.blastService = new GraphBlastService('ws://127.0.0.1:8081/blast');

        // Step 2 - attach graph's collection 'deals' to local array
        this.blastService.attach('deals', this.deals);
    }
}    
```
And to render the local copy, here is an HTML snippet. When the data on the server changes, the 'deals' ngModel is automatically updated and the display (reacts) accordingly:

```
    <div *ngFor="let deal of deals" >
        <img src="{{deal.product.image}}" />
        <div>{{deal.product.name}}</div>
        <div>{{deal.product.description}}</div>
    </div>
```



#Server Code

```
 // create a new Blast Graph
 BlastGraph blastGraph = new BlastGraphImpl();

 // create the graph module with the graph
 GraphModule graphModule = new GraphModule(blastGraph);

 // configure the blast server with the graph module
 BlastServer blast = Blast.blast(new VertxEngine(), graphModule);

 // add server to blast graph
 blastGraph.setBlastServer(blast);

```

